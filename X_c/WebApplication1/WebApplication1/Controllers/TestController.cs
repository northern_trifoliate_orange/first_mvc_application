﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WebApplication1.Controllers
{
    public class TestController : Controller
    {
        // GET: Test
        public ActionResult ABC()
        {
            return View();
        }
        public string Hello(string ip,string name) 
        {
            string str = string.Format("你的id={0},你的名字叫{1}",ip,name);
            return str;
        }
        public string Hi(int id)
        {
            string str = string.Format("你的id={0}", id);
            return str;
        }
    }
}