﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WebApplication1.Controllers
{
    public class TestController : Controller
    {
        // GET: test
        public ActionResult Index()
        {
            var str = "泰罗";
            ViewBag.Message = str;
            return View();
        }
        public string Hello( string ip,string name)
        {
            string res = string.Format("你好,{0}打怪兽,{1}", ip,name );
            return res;
        }
        public string Hi(int id )
        {
            string res = string.Format("Hi,你的id={0}",id);
            return res;
        }
    }
}