﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WebApplication1.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "被历史大漠掩盖的传奇故事";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "联系人页面";

            return View();
        }
    }
}