﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WebApplication1.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public string About()
        {
            var str = "我是李育奇";
            ViewBag.Message = str;

            return str;
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "聪明人看的见的联系方式";

            return View();
        }
    }
}