﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MVC_1.Controllers
{
    public class TestController:Controller
    {
        //无参构造
        public ActionResult aaa()
        {
            var a = "hello world";
            return View();
        }

        //mvc地址传参
       public string bbb(string ip)
        {
            var b = string.Format("你的IP：{0}",ip);//可以为空，可以不传参的值，只能使用“？”
            return b;
        }

        public string ddd(int id)
        {
            var d = string.Format("你的id：{0}",id);//id 可以使用“/”和“？”路径传参的方式，但必须先给参值
            return d;
        }

        public string ggg(int age)
        {
            var g = string.Format("年龄：{0}", age);//必须先传参的值,int类型，否则报错，只能使用“？”
            return g;
        }

        public string ccc(string ip,string HostName)
        {
            var c = string.Format("主机的IP地址：{0}，主机名称：{1}",ip,HostName);
            return c;
        }


        public string fff(int id ,string name)
        {
            var f = string.Format("ID：{0},名字；{1}",id,name);
            return f;
        }
    }
}

// 两种地址传参的区别