﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MVC_1.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public string About()
        {
            var str = "这是一个神奇的东西";
            ViewBag.Message = str;
            return str;
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "猜到我就告诉你";

            return View();
        }
    }
}