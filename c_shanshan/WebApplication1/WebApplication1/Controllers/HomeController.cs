﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WebApplication1.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public string About()
        {
            var str = "最想拥有的汉服是明华堂家的";
            ViewBag.Message = str;

            return str;
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "我的联系方式：去问万宝德的掌柜";

            return View();
        }
    }
}