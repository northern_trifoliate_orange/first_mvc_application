﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WebApplication1.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "中华人民共和国";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "想知道不会告诉你";

            return View();
        }
    }
}