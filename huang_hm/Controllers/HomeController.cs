﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WebApplication3.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public string About()
        {
            var str = "黄慧敏超好看";

            ViewBag.Message = str;

            return str;
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "你真棒！";

            return View();
        }
    }
}