﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WebApplication3.Controllers
{
    public class TestController: Controller
    {
        public ActionResult BBC()
        {
            var str = "今天天气好耶哦";
            return View();
        }

        public string Hello(string ip,string name)
        {
            string res = string.Format("你好,{0},我是你的{1}",ip,name);
            return res;
        }

        public string Hi(int id)
        {
            string res = string.Format("Hi,你的Id={0}",id);
            return res;
        }
    }
}