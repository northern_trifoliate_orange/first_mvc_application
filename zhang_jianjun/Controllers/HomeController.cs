﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WebApplication2.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public string About()
        {
            var str = "小章的小弟";
            ViewBag.Message = str;
            return str;
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "想要我的联系方式吗？";

            return View();
        }
    }
}