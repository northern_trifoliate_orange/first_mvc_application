﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace test1.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "关于mvc的一切还都不明白";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "联系方式就不告诉你啦";

            return View();
        }
    }
}