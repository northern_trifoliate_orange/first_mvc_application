﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WebApplication3.Controllers
{
    public class TestController : Controller
    {
        public ActionResult ABC()
        {
            var str = "天下掉下个猪八戒";

            return View();
        }

        public string HEY(string ip,string name)
        {
            string res = string.Format("今天天气{0},我想吃{1}", ip, name); //问号输入：HEY?IP=真好&&name=冰淇淋

            return res;
        }


        public string  Hi(int id)
        {
            string res = string.Format("Hi,我的学号{0}", id);//斜杆输入：Hi/22

            return res;
        }
        

    }
}