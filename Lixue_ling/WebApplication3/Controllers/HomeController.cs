﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WebApplication3.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public string About()
        {
            var str = "这是关于雪玲的部落格 -- 生活原本沉闷，但跑起来就会有风。";

            ViewBag.Message = str;

            return str;
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "告辞！";

            return View();
        }
    }
}