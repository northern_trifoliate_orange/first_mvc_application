﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WebApplication1.Controllers
{
    public class TestController:Controller 
    {
        public ActionResult  BBQ()
        {
            var str = "我爱我的祖国";

            return View();
        }

        public string Hello(string ip,string name)
        {
            string res = string.Format("你好，{0},我叫{1}", ip,name);
            return res;
        }

        public string Hi(int id,string name)
        {
            string res = string.Format("Hi,你的Id = {0},姓名= {1}", id,name);
            return res;
        }
    }
}