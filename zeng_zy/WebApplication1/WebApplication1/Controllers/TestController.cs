﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WebApplication1.Controllers
{
    public class TestController:Controller
    {
        public ActionResult BBC()
        {
            var str= "欢迎来到中国";
            return View();
        }
        public string Hello(string ip,string name)
        {
            var str = string.Format("你好,{0},我是，{1}",ip,name);
            return str;
        }
        public string Student(string id)
        {
            var studentId = string.Format("你的id是{0}", id);
            return studentId;
        }
    }
}