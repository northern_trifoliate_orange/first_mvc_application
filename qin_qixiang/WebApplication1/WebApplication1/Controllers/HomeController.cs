﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WebApplication1.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public string About()
        {
            var str = "？？？";

            ViewBag.Message = str;

            return str;
        }

        public string Contact()
        {
            var str = "联系方式：？？？";

            ViewBag.Message = str;

            return str;
        }
    }
}