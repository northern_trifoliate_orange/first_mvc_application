﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WebApplication1.Controllers
{
    public class TestController : Controller
    {
        public ActionResult BBC()
        {
            var str = "我的心情";
            return View();
        }

        public string Hello(string ip,string name) 
        {
            string res = string.Format("应该还可以,{0}", ip,name);
            return res;
        }
        public string Hi(int id)
        {
            string res = string.Format("Hi,你的ip地址={}", id);
            return res;
        }
    }
}