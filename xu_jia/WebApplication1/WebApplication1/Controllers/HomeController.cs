﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WebApplication1.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "第一次测试";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "应该成功";

            return View();
        }
    }
}