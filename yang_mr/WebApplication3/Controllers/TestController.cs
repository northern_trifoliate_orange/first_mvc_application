﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WebApplication3.Controllers
{
    public class TestController : Controller
    {
        // GET: Test
        public ActionResult Index()
        {
            return View();
        }
        public string Destire(string province)
        {
            //province 省份
            //GET  Test/Destire?province=xx
            string pro = string.Format("咳咳，我系{0}嘎😁😁", province);
            return pro;
        }
        public string Text(string age, string name)
        {
            //GET  Test/Text?age=xx&name=xxx
            string tes = string.Format("咳咳，今年{0}岁,叫我{1}就好😁😁", age, name);
            return tes;
        }

        public ActionResult PostData()
        {
            return Content(Request.Form["log"]);
        }
    }
}